from flask import Flask
from flask import render_template
import os


app = Flask(__name__)

@app.route("/")
def spash():
    return render_template('splash.html')

@app.route("/<path>")
def valid(path):
    if (("~" in path) or (".." in path) or ("//" in path)):
        return render_template('403.html'),403
    if os.path.isfile("templates/" + path):
        return render_template(path),200
    else:
        return render_template('404.html'),404
            
@app.errorhandler(403)
def fnf(arg):
   return render_template('403.html'),403

@app.errorhandler(404)
def ffb(arg):
    return render_template('404.html'),404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
